<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Integrasi_sso extends CI_Controller
{
    
    var $sso_host = 'http://51.79.230.10:11567/api/checkSSO';
    var $sso_logout = 'http://51.79.230.10:11567/api/logout';
    var $sso_client_id = 'SSO-ti-7634cb1d31be8691fcd08d03d335c8ad';
    var $sso_client_secret = '66cfa499dcb7d6a2ddf344565c13e9867fedfd45ce351c09b6f49b0f88da3d8f';

    public function index()
    {
        // Mengambil token dari query parameter
        $token = $this->input->get('token');

        // Variable untuk refresh token atau tidak [true=refresh token , false=tidak refresh token]
        $refreshToken = false;

        // Definisikan variable untuk header
        $headers = array(
            "Authorization: Bearer " . $token,
            "SSO-Client-ID: " . $this->sso_client_id,
            "SSO-Client-Secret: " . $this->sso_client_secret,
            "SSO-Refresh:" . $refreshToken,
        );

        // Melakukan request ke sso_host untuk mengecek token
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->sso_host);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $server_output = curl_exec($ch);

        curl_close($ch);
       
        // Mengambil json dari http request dan di convert ke array
        $res = json_decode($server_output, true);
       
        if ($res['meta']['code'] == 200) {
         // Jika ntegrasi SSO BERHASIL

            // Aksi lanjutan jika token berhasil di cek dan response code nya 200

       
        } else {
        // Jika Integrasi SSO Gagal 

        }
    }

    public function logout() {
         // Mengambil token dari query parameter
         $token = $this->input->get('token');
 
         // Definisikan variable untuk header
         $headers = array(
             "Authorization: Bearer " . $token,
         );

         $ch = curl_init();
         curl_setopt($ch, CURLOPT_URL, $this->sso_logout);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
 
         $server_output = curl_exec($ch);
 
         curl_close($ch);
        
         // Mengambil json dari http request dan di convert ke array
         $res = json_decode($server_output, true);

         if ($res['meta']['code'] == 200) {
            // Jika integrasi SSO BERHASIL
   
               // Aksi lanjutan jika berhasil dan response code nya 200
   
          
           } else {
           // Jika Integrasi SSO Gagal 
   
           }
    }
}
